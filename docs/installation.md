---
title: Installation
hide_title: true
---

# Installation steps for CredCLI

1. Extract zip.
2. Place the extracted ‘credcli’ folder to somewhere safe on your disk.
   Make sure you don’t move or change the contents of the folder later.
3. Fire up a terminal and go to the path of the credcli folder.
4. Run `npm link --force` . It will start the installation process.

   ![npm link](./media/npmLink.png)

   Once done, run credcli from any location and you should get some output like this.

   ![installed](./media/installed.png)

# Setting-up Microservices

- **_For standard configuration_**

1. Create an .env file under your folder which contains all or most of the services.

2. Inside your env file specify all your services (and application name, if present)

   ![env](./media/env.png)

3. Run `credcli patch:init`

- **_For docker configuration_**

1. Place docker-compose file which you got for initialization inside your microservice folder.

2. Create or import `prod-config` folder

3. Run `credcli patch:init`

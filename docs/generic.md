---
title: Generic
hide_title: true
---

1. Add `.gitkeep` file inside the folders which are intended to be kept in the system even when they are empty because empty folders will be deleted by credcli.

2. Make sure you have folders available for all the services affected by manual scripts. If there are no modified files for a service, please supply an empty folder inside the patch.

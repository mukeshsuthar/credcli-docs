---
title: Introduction
hide_title: true
---

# CRED-CLI

Credence CLI Application

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)

- [Installation](installation.md/#installation-steps-for-credcli)
- [Usage](#usage)
- [Commands](#commands)
  - [Macro](macro.md)
    - [`credcli macro:add`](macro.md/#credcli-macroadd)
    - [`credcli macro:export`](macro.md/#credcli-macroexport)
    - [`credcli macro:import`](macro.md/#credcli-macroimport)
    - [`credcli macro:remove`](macro.md/#credcli-macroremove)
  - [Patch](standard.md)
    - [`credcli patch:init`](standard.md/#credcli-patchinit)
    - [`credcli patch:apply`](standard.md/#credcli-patchapply)
    - [`credcli patch:pull`](docker.md/#credcli-patchpull)
    - [`credcli patch:push`](docker.md/#credcli-patchpush)
    - [`credcli patch:info`](standard.md/#credcli-patchinfo)
    - [`credcli patch:revert`](standard.md/#credcli-patchrevert)
    - [`credcli patch:create`](standard.md/#credcli-patchcreate)
- [Manual Scripts](manualScript.md/#manual-scripts)

# Usage

```sh
$ npm install -g @credenceanalytics/credcli

$ credcli COMMAND
running command...

$ credcli (-v|--version|version)
credcli/1.2.0 darwin-x64 node-v10.17.0

$ credcli --help [COMMAND]
USAGE
  $ credcli COMMAND
...
```

# Commands

- [Macro](macro.md)
- [Patch](standard.md)

- Get help for any _credcli command_

  ```
  USAGE
    $ credcli help [COMMAND]

  ARGUMENTS
    COMMAND  command to show help for

  OPTIONS
    --all  see all commands in CLI
  ```

  > _See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.1.0/src/commands/help.ts)_

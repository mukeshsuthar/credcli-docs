---
id: standard
title: Standard
hide_title: true
---

- [`credcli patch:init`](#credcli-patchinit)
- [`credcli patch:apply`](#credcli-patchapply)
- [`credcli patch:info`](#credcli-patchinfo)
- [`credcli patch:revert`](#credcli-patchrevert)
- [`credcli patch:create`](#credcli-patchcreate)

### `credcli patch:init`

- **_First initialization_**

![](./media/init/init.gif)

- **_Add or update service_**

_Please create an empty folder while adding a new service and then run this command._

![](media/init/init-add-update.gif)

- **_Add encryption keys_**

  If you have encrypted files, then please provide encryption keys so that those files can be updated. Run `credcli patch:init` and specify location of _public key_ and _private key_.

To initialize the patch configuration in the root folder

```
USAGE
  $ credcli patch:init
```

### `credcli patch:apply`

- **_Apply single patch_**

![](media/apply/apply.gif)

- **_Apply bulk patch_**

![](media/apply/bulk-apply.gif)

- **_Re-apply an already applied patch_**

![](media/apply/re-apply.gif)

Applies the patch on your system

```
USAGE
  $ credcli patch:apply

DESCRIPTION
  Users can either add patch file inside patchFolder and select that patch, or enter the path of the file.
```

### `credcli patch:info`

- **_Services and patch installed_**

![](media/info/info.gif)

- **_History of patches_**

![](media/info/info-details.gif)

Gives information on the current patch installed on the system

```
USAGE
  $ credcli patch:info
```

### `credcli patch:revert`

![](media/revert/revert.gif)

Restores the patch to previous version

```
USAGE
  $ credcli patch:revert
```

### `credcli patch:create`

Creates a new patch based on difference between two branches of a service.

```
USAGE
  $ credcli patch:create

OPTIONS
  -b, --baseBranch=baseBranch    old patch branch
  -p, --patchBranch=patchBranch  new patch branch

```

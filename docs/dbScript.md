---
title: Database Scripts
hide_title: true
---

**Rules that needs to be followed while creating Database Scripts for a patch**.

1. Script file should be named either _**dbcript.sql**_ or _**revertscript.sql**_

   - _**dbcript.sql**_: Script containing sql statements.
   - _**revertscript.sql**_: Script to revert if _**dbcript.sql**_ fails.

2. Both of those scripts should be present inside the patch.

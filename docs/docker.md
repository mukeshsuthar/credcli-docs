---
title: Docker
hide_title: true
---

- [`credcli patch:init`](#credcli-patchinit)
- [`credcli patch:pull`](#credcli-patchpull)
- [`credcli patch:push`](#credcli-patchpush)
- [`credcli patch:info`](#credcli-patchinfo)
- [`credcli patch:revert`](#credcli-patchrevert)
- [`credcli patch:create`](#credcli-patchcreate)

### `credcli patch:init`

<!-- ![](media/init/init-add-update.gif) -->

To initialize the patch configuration in the root folder

```
USAGE
  $ credcli patch:init
```

- **_Add encryption keys_**

  If you have encrypted files, then please provide encryption keys so that those files can be updated. Run `credcli patch:init` and specify location of _public key_ and _private key_.

### `credcli patch:pull`

<!-- ![](media/apply/apply.gif) -->

Applies the patch on your system

```
USAGE
  $ credcli patch:pull

DESCRIPTION
  Users can either add patch file inside patchFolder and select that patch, or enter the path of the file.
```

### `credcli patch:push`

<!-- ![](media/apply/apply.gif) -->

Pushes images to the push repository

```
USAGE
  $ credcli patch:push
```

### `credcli patch:info`

- **_Services and patch installed_**

  <!-- ![](media/info/info.gif) -->

Gives information on the current patch installed on the system

```
USAGE
  $ credcli patch:info
```

- **_History of patches_**

<!-- ![](media/info/info-details.gif) -->

Gives information on the past version of patches installed

```
USAGE
  $ credcli patch:info
```

### `credcli patch:revert`

<!-- ![](media/revert/revert.gif) -->

Restores the patch to previous version

```
USAGE
  $ credcli patch:revert
```

### `credcli patch:create`

Creates a new patch based on difference between two branches of a service.

```
USAGE
  $ credcli patch:create

OPTIONS
  -b, --baseBranch=baseBranch    old patch branch
  -p, --patchBranch=patchBranch  new patch branch

```

---
title: Macro
hide_title: true
---

- [`credcli macro:add`](#credcli-macroadd)
- [`credcli macro:export`](#credcli-macroexport)
- [`credcli macro:import`](#credcli-macroimport)
- [`credcli macro:remove`](#credcli-macroremove)

## Macro

### `credcli macro:add`

Adds new configuration for the macro and creates desired folder and file

```
USAGE
  $ credcli macro:add

OPTIONS
  -a, --app=app        app in which the macro should be displayed
  -f, --folder=folder  path to the macro folder
  -l, --label=label    label for the action
  -n, --name=name      name to the macro
  -r, --route=route    specific application route in which this macro should be set

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/macro/add.js](https://gitlab.com/wealthnfunds/credcli/blob/v1.2.0/src/commands/macro/add.js)_

### `credcli macro:export`

Exports a macro to a json file in current directory

```
USAGE
  $ credcli macro:export

OPTIONS
  -n, --name=name  (required) name to the macro
```

_See code: [src/commands/macro/export.js](https://gitlab.com/wealthnfunds/credcli/blob/v1.2.0/src/commands/macro/export.js)_

### `credcli macro:import`

Imports the macro file to the current configuration

```
USAGE
  $ credcli macro:import

OPTIONS
  -f, --file=file  json configuration file which you want to import
```

_See code: [src/commands/macro/import.js](https://gitlab.com/wealthnfunds/credcli/blob/v1.2.0/src/commands/macro/import.js)_

### `credcli macro:remove`

Removes a macro from the configuration and also the respective folder

```
USAGE
  $ credcli macro:remove

OPTIONS
  -f, --folder=folder  path to the macro folder
  -n, --name=name      name to the macro
```

_See code: [src/commands/macro/remove.js](https://gitlab.com/wealthnfunds/credcli/blob/v1.2.0/src/commands/macro/remove.js)_

---
title: Manual Scripts
hide_title: true
---

**Here are some set of rules that needs to be followed while creating manual scripts for a patch**.

1. Script file should be named either _**prescript.js**_ or _**postscript.js**_

   - _**prescript.js**_: if the script needs to be executed before applying the patch
   - _**postscript.js**_: if the scripts needs to be executed after applying the patch

2. List of available methods inside a script file is following:

   - **'@credenceanalytics/parsifier'** - [ updateJson, appendJson, deleteJson, updateXml, appendXml, updateJds, appendJds, appendEnv, updateEnv, updateJs, appnedJs ]
   - **fs** - [ unlinkSync, rmdirSync, renameSync ]
     > _Refer [***@credenceanalytics/parsifier***](https://www.npmjs.com/package/@credenceanalytics/parsifier) and [***fs***](https://nodejs.org/api/fs.html) to know more about the specific usage._

3. You can only use these three libraries inside a script i.e. **path, fs, @credenceanalytics/parsifier**

4. Please supply realtive path as an argument ex:`'./ServiceName/ContentToChange'`

5. You can only modify content inside the folders of services.

6. Make sure you have folders available for all the services affected by manual scripts. If there are no modified files for a service, please supply an empty folder inside the patch.

7. While changing configurations, pass an additional argument `true` if the file is _encrypted_.

Example

```javascript
const parsifier = require("@credenceanalytics/parsifier");
const fs = require("fs");
parsifier.updateJson("./nrest/hello.json", "version", 1.2);
// to modify an encrypted file
parsifier.updateJson("./nrest/hello.json", "version", 1.2, true);
// to delete a file
fs.unlinkSync("./nrest/cv.js");
```

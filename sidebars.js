module.exports = {
  someSidebar: {
    "Cred-CLI":['intro','installation','macro',
    {
      type: 'category',
      label: 'Patch',
      items: ['standard','docker'],
      },
      {
      type: 'category',
      label: 'Development',
      items: ['manualScript','dbScript', 'generic'],
      }
    ],
  },
};
